<?php
namespace supevents\Toastr;

class Toastr {

    public static $msgss = array();

    public static function add($type = 'info', $position = 'top-full-width', $message = false, $title = false){
        if(!$message) return false;
        if(is_array($message)){
            foreach($message as $msg){
                static::$msgss[$type][$position][$title][] = $msg;
            }
        }
        else{
            static::$msgss[$type][$position][$title][] = $message;
        }
        \Session::flash('toastrs', static::$msgss);
    }

    public static function get(){
        $messages = \Session::get('toastrs');
        $output = '';
        if($messages){
            $output = '<script>';
            foreach($messages as $type=>$array1)
                foreach($array1 as $position=>$array2)
                    foreach($array2 as $title=>$array3)
                        foreach($array3 as $key=>$message){
                            $output .= 'toastr.options = {"positionClass": "toast-'.$position.'" };';
                            $output .= 'toastr["'.$type.'"]("'.$title.'", "'.$message.'");';
                        }
            return $output.'</script>';
        }
        return $output;
    }
}