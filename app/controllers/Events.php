<?php

class Events extends BaseController {

    public function __construct(){
        $this->beforeFilter('auth');
    }

    public function getIndex()
    {
        $this->data['futureEvents'] = Auth::user()->getFutureEvents()->orderBy('from','asc')->get();
        $this->data['pastEvents'] = Auth::user()->getPastEvents()->orderBy('from','asc')->get();
        return View::make('events.index',$this->data);
    }

    public function getAdd()
    {
        $me = Auth::user();
        $this->data['users'] = User::where('id','!=',$me->id)->orderBy('prenom','asc')->get();
        $this->data['eventTypes'] = EvenementType::orderBy('id','asc')->get();
        return View::make('events.add',$this->data);
    }

    public function getView($id = false)
    {
        $me = Auth::user();
        $this->data['isInvited'] = false;
        $event = Evenement::find($id);
        foreach($event->invitations()->get() as $invitation)
            if(($invitation->user()->first() == Auth::user()) && ($invitation->result == 0))
                $this->data['isInvited'] = true;

        $this->data['futureEvents'] = Auth::user()->getFutureEvents()->orderBy('from','asc')->get();
        $this->data['pastEvents'] = Auth::user()->getPastEvents()->orderBy('from','asc')->get();
        $this->data['users'] = User::where('id','!=',$me->id)->orderBy('prenom','asc')->get();
        $this->data['event'] = $event;
        return View::make('events.view',$this->data);
    }

    public function postAdd()
    {
        $user = Auth::user();
        $rules = array(
            'name' => 'required',
            'description' => 'required',
            'address' => 'required',
            'from-d' => 'required',
            'from-h' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
            return Redirect::back()->with('errors',$validator->messages());
        } else {
            $event = new Evenement();
            $event->name = Input::get('name');
            $event->description = Input::get('description');
            $event->address = Input::get('address');
            $event->details = Input::get('details');
            $type = EvenementType::find(Input::get('type'));
            $event->type()->associate($type);
            if(Input::get('privacy') == 'on')
                $event->privacy = 0; //privacy 0 = ouvert public
            else
                $event->privacy = 1; //privacy 1 = fermé privé

            $format = 'm/d/Y H:i';
            $dateF = DateTime::createFromFormat($format, Input::get('from-d').' '.Input::get('from-h'));
            $event->from = $dateF->format('Y-m-d H:i:s');
            if(Input::has('to-h') && Input::has('to-d')){
                $dateT = DateTime::createFromFormat($format, Input::get('to-d').' '.Input::get('to-h'));
                $event->to = $dateT->format('Y-m-d H:i:s');
            }
            $user->evenements()->save($event);
            $event->users()->save($user);

            if(Input::has('users')){
                foreach(Input::get('users') as $uid){
                    $invitation = new Invitation();
                    $invitation->evenement()->associate($event);
                    $userInvited = User::find($uid);
                    $invitation->user()->associate($userInvited);
                    $invitation->save();

                    $notification = new Notification();
                    $notification->link = "/events/view/".$event->id;
                    $notification->user()->associate($userInvited);
                    $notification->text = Auth::user()->getName().' invited you to '.$event->name.'.<br> Please accept or deny.<br>';
                    $notification->save();
                }
            }

            return Redirect::to('/events');
        }
    }

    public function getAccept($id = false){
        $event = Evenement::find($id);
        $user = Auth::user();

        foreach($user->invitations()->get() as $invitation)
            if($invitation->evenement()->first() == $event){
                $invitation->result = 1; //Accepté
                $invitation->save();
                $event->users()->save($user);
                $notification = new Notification();
                $notification->link = "/profile/show/".$user->id;
                $notification->user()->associate($event->user()->first());
                $notification->text = $user->getName().' has accepted your invitation to '.$event->name.'.';
                $notification->save();
            }

        return Redirect::to('/events/view/'.$event->id);
    }


    public function getDeny($id = false){
        $event = Evenement::find($id);
        $user = Auth::user();

        foreach($user->invitations()->get() as $invitation)
            if($invitation->evenement()->first() == $event){
                $invitation->result = 2; //Refusé
                $invitation->save();
                $notification = new Notification();
                $notification->link = "/profile/show/".$user->id;
                $notification->user()->associate($event->user()->first());
                $notification->text = $user->getName().' has refused your invitation to '.$event->name.'.';
                $notification->save();
            }

        return Redirect::to('/events/view/'.$event->id);
    }

    public function postAddinvitations()
    {
        $event = Evenement::find(Input::get('id'));
        if(Input::has('users')){
            foreach(Input::get('users') as $uid){
                $invitation = new Invitation();
                $invitation->evenement()->associate($event);
                $userInvited = User::find($uid);
                $invitation->user()->associate($userInvited);
                $invitation->save();

                $notification = new Notification();
                $notification->link = "/events/view/".$event->id;
                $notification->user()->associate($userInvited);
                $notification->text = Auth::user()->getName().' invited you to '.$event->name.'.<br> Please accept or deny.<br>';
                $notification->save();
            }

            Toastr::add('success','top-full-width','Event updated!','The event has been successfully updated');
            return Redirect::back();
        }
        return Redirect::back();
    }

}