<?php

//Resources
Route::resource('user','User');

//Controllers
Route::controller('profile','Profile');
Route::controller('events','Events');
Route::controller('friends','Friends');


//Root controller
Route::controller('/','Home'); //Laisser en dernier

// Route::post('/login',array('uses'=>'home@login'));
// Route::get('/register',array('uses'=>'home@register'));
// Route::get('/logout',array('uses'=>'home@logout'));

