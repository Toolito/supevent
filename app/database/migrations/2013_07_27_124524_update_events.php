<?php

use Illuminate\Database\Migrations\Migration;

class UpdateEvents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
      Schema::table('events', function($table)
      {
         $table->text('details')->after('address');
         $table->dateTime('from');
         $table->dateTime('to');
      });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
      Schema::drop('events');
	}

}