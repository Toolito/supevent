<?php
class Activity extends Eloquent
{
    protected $table = "activities";

    public function user(){
        return $this->belongsTo('User');
    }
}