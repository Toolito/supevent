@extends('layout.main')
@section('title')
Add a friend
@endsection
@section('subtitle')
Friends
@endsection
@section('content')

<legend>Looking for friends ?</legend>
<div class="row-fluid search-forms search-default">
    <!--<form class="form-search" action="#">-->
            <input name="firstName"  type="text" placeholder="First Name" class="m-wrap" />
            <input name="lastName" type="text" placeholder="Last Name" class="m-wrap" />
            <input name="username" type="text" placeholder="Username" class="m-wrap" />
            <select name="campus" class="m-wrap">
                <option value="null">Choose a campus</option>
                @foreach($campus as $c)
                <option value="{{$c->id}}">{{$c->name}} ({{$c->country->name}})</option>
                @endforeach
            </select>
            <button id="search" type="button" class="pull-right btn green">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
    <!--</form>-->
</div>
<div id="results" style="display: none" class="portlet-body">
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
$( document ).ready(function() {
    $('#search').click(function(e) {
		e.preventDefault();
	    $(this).each(function() {
	        $.post('/friends/results', {
	            firstName: $('input[name=firstName]').val(),
	            lastName: $('input[name=lastName]').val(),
	            username: $('input[name=username]').val(),
	            campus: $('select[name=campus]').val()
	        }, function(data){
            	var html = "";
            	$.each(data, function(z,obj){
            		html += "<tr>";
            		html += "<td>"+obj.id+"</td>";
            		html += "<td>"+obj.prenom+"</td>";
            		html += "<td>"+obj.nom+"</td>";
            		html += "<td>"+obj.username+"</td>";
            		html += "<td>";
            		html += '<a href="/friends/chfriend/'+obj.id+'" class="btn green-stripe mini" rel="'+obj.id+'">Add/Remove</a>';
            		html += "</td>";
            		html += "</tr>";
		        });
            	$('tbody').empty().append(html);

            	$('#results').show();
	    	});
	    });
	});
});

</script>
@endsection