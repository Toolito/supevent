@include('layout.head') <!-- HTML doctype, head et body ouvrante -->
<body class="page-header-fixed">
@include('layout.header') <!-- Logo, blue navbar, user infos -->
<div class="page-container row-fluid">
	@include('layout.sidebar')
	@include('layout.content-header')
						@yield('content')
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>

	@include('layout.footer')
	
	@include('inc.scripts')
	
</body>
</html>