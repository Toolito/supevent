<ul class="breadcrumb">
	<li>
      <i class="icon-home"></i>
      <a href="/">Home</a> 
      <span class="icon-angle-right"></span>
   </li>
	<li>
		<a href="{{URL::current()}}">@yield('title')</a>
	</li>
    <li class="pull-right no-text-shadow">
        <div style="display: block;" id="dashboard-report-range" class="dashboard-date-range responsive">
            <i class="icon-calendar"></i>
            <span></span>
        </div>
    </li>
</ul>