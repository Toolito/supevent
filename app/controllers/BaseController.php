<?php

class BaseController extends Controller {

	public $data = array();

    public function __construct(){

        if(Auth::user()->invitations()->count() > 0){
            foreach(Auth::user()->invitations() as $invitation){
                Toastr::add('info',
                    'top-right',
                    'You are invited!',
                    ''.$invitation->evenement->user->getName().' invited you to '.$invitation->evenement->name.'.<br> Please accept or deny.<br><a class="btn green btn-xs" href="/events/accept/'.$invitation->id.'">Accept</a><a class="btn red btn-xs" href="/events/deny/'.$invitation->id.'">Deny</a>'
                );
            }
        }
    }
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}