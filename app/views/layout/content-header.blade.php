		<!-- BEGIN PAGE -->
		<div class="page-content" style="min-height:998px !important">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div style="display: none;" class="color-mode-icons icon-color-close"></div>
							<div style="display: none;" class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
@if(Auth::user()->theme == 'default')<li class="switch-col color-black color-default current" data-style="default"></li>
									@else
									<li class="switch-col color-black color-default" data-style="default"></li>
									@endif
@if(Auth::user()->theme == 'blue')	<li class="switch-col color-blue current" data-style="blue"></li>
									@else
									<li class="switch-col color-blue" data-style="blue"></li>
									@endif
@if(Auth::user()->theme == 'brown')	<li class="switch-col color-brown current" data-style="brown"></li>
									@else
									<li class="switch-col color-brown" data-style="brown"></li>
									@endif
@if(Auth::user()->theme == 'purple')	<li class="switch-col color-purple current" data-style="purple"></li>
									@else
									<li class="switch-col color-purple" data-style="purple"></li>
									@endif
@if(Auth::user()->theme == 'light')	<li class="switch-col color-white color-light current" data-style="light"></li>
									@else
									<li class="switch-col color-white color-light" data-style="light"></li>
									@endif
								</ul>
								<label class="hidden-phone">
								<div id="uniform-undefined" class="checker"><span class="checked"><input style="opacity: 0;" class="header" checked="" value="" type="checkbox"></span></div>
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>
						<h3 class="page-title">@yield('title')
							<small>@yield('subtitle')</small>
						</h3>
						<!-- END STYLE CUSTOMIZER-->  
						<?=View::make('layout.breadcrumbs')?>
					</div>
				</div>
