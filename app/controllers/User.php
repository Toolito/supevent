<?php

class User extends BaseController {

    public function __construct(){
        $this->beforeFilter('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return 'index';
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return 'hello';
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$user = new User;
		$user->username = Input::get('username');
		$user->set_password(Input::get('password'));
		$test_supinfo = '@supinfo.com';
		if(stristr(Input::get('email'),$test_supinfo))
		{
			$rest = substr(Input::get('email'),0,-12);
			if(is_numeric($rest))
			{
				$student = new Student;
				$student->opencampus_id = $rest;
				$student->save();
			}
			else{}
		}
		$user->email = Input::get('email');
		$user->save();
		$student->user()->insert($user);
		return Redirect::to('login');	
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return 'hello '.$id;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}