<?php
class Campus extends Eloquent
{
	protected $table = "campus";

	public function country()
	{
		return $this->belongsTo('Country','country_id');
	}
}