<?php
class Invitation extends Eloquent
{
    public function user()
    {
        return $this->belongsTo('User', 'dest_id');
    }

    public function evenement()
    {
        return $this->belongsTo('Evenement', 'event_id');
    }

    public function result(){
        switch($this->result){
            case 0:
                return 'N/A';
            break;
            case 1:
                return 'Yes';
            break;
            case 2:
                return 'No';
            break;
        }
    }
}