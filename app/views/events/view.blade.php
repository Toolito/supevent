@extends('layout.main')
@section('title')
Events
@endsection
@section('subtitle')
view
@endsection
@section('content')
<div class="row-fluid inbox">
    @include('shared.myevents')
    <div id="default" class="span10">
        <div class="inbox-header">
            <h1 id="eventName" class="pull-left">{{$event->name}}</h1><br>
            <small style="font-size: 1.3em" class="page-title" id="#eventCreator">by {{$event->user->getName()}} •
                @if($event->privacy == 0) <i data-placement="top" data-original-title="P u b l i c" class="tooltips icon-globe"></i>
                @else <i data-placement="top" data-original-title="P r i v a t e" class="tooltips icon-lock"></i>
                @endif
            </small>
        </div>
        <div class="inbox-content">
            <div class="row-fluid">
                <div class="span6">
                    <p class="lead">{{$event->description}}</p>
                    <ul class="unstyled">
                        <li>
                            <h4><small>Type:</small> {{$event->type->name}}</h4>
                        </li>
                    </ul>
                </div>
                <div class="span6">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">Localization</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            {{$event->address}}<br>{{$event->details}}
                            <input type="hidden" id="gmap_geocoding_address" value="{{$event->address}}"/>
                            <div id="gmap_view" class="gmaps">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(count($event->users) > 0)
            <div class="row-fluid">
                <div class="span6">

                </div>
                <div class="span6">
                    <div class="tabbable tabbable-custom tabbable-custom-profile">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th><span class="lead">Going</span> <span class="pull-right badge badge-success">{{count($event->users)}}</span></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($event->users as $u)
                            <tr>
                                <td>
                                    <a href="/profile/show/{{$u->id}}">{{$u->getName()}}</a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
            @if($event->getInvitedCount() > 0)
            <div class="row-fluid">
                <div class="span6">

                </div>
                <div class="span6">
                    <div class="tabbable tabbable-custom tabbable-custom-profile">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th><span class="lead">Invited</span> <span class="pull-right badge badge-success">{{count($event->getInvitedWoResult())}}</span></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($event->getInvitedWoResult() as $u)
                            <tr>
                                <td>
                                    <a href="/profile/show/{{$u->id}}">{{$u->getName()}}</a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
            @if($event->isAuthCreator())
            <div class="row-fluid">
                <div class="span12">
                    {{Form::open(array('url'=>'/events/addinvitations', 'method'=>'POST','class'=>'form-horizontal'))}}
                    <fieldset>
                        <legend>Add invitations</legend>
                        <label>Users to invite</label>
                        <select name="users[]" id="users_list" class="span6 select2" multiple>
                            @foreach($users as $us)
                            @if($us->prenom == "" || $us->nom == '')
                            <option value="{{$us->id}}">{{$us->username}}</option>
                            @else
                            <option value="{{$us->id}}">{{$us->prenom}} {{$us->nom}}</option>
                            @endif
                            @endforeach
                        </select>
                        <input type="hidden" name="id" value="{{$event->id}}"/>
                    </fieldset>
                    <div class="form-actions">
                        <button class="pull-right btn green" type="submit">Update <i class="m-icon-swapright m-icon-white"></i></button>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

@endsection
@section('styles')
<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="/assets/plugins/glyphicons/css/glyphicons.css" />
<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
<link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link href="/assets/css/pages/inbox.css" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="/assets/plugins/jquery.pulsate.min.js"></script>
<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="/assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
<script src="/assets/plugins/gmaps/gmaps.js"></script>
<!--END PAGE LEVEL PLUGINS -->
<script src="/assets/scripts/maps-google.js"></script>
<script src="/assets/scripts/inbox.js"></script>
<script type="text/javascript">
    $(function(){
        MapsGoogle.mapView();
        $('.active').append('<b></b>');
        $('#users_list').select2({
            placeholder: "Select users",
            allowClear: true
        });
    });

</script>
@if($isInvited)
<script type="text/javascript">
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-full-width",
        "onclick": null,
        "showDuration": "0",
        "hideDuration": "0",
        "timeOut": "0",
        "extendedTimeOut": "0",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    toastr["success"]("<a class='btn green' href='/events/accept/{{$event->id}}'>Accept</a> <a class='btn red' href='/events/deny/{{$event->id}}'>Deny</a>", "You are invited!");
</script>

@endif
@endsection
