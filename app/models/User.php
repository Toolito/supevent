<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function set_password($password)
	{
		$this->setAttribute('password',Hash::make($password));
	}
	
	public function set_email($email)
	{
		$this->setAttribute('email',$email);
	}

	public function country()
	{
		return $this->belongsTo('Country','country_id');
	}

	public function student()
	{
		return $this->belongsTo('student');
	}

    public function evenements()
    {
        return $this->hasMany('evenement');
    }

    public function allevenements()
    {
        return $this->belongsToMany('evenement', 'user-event', 'user_id', 'event_id');
    }

    public function friends()
    {
    	return $this->belongsToMany('User', 'user-friend', 'user_id', 'friend_id');
    }

    public function getNoFriends()
    {
        $friends = $this->friends()->get();
        $users = User::all();

        foreach($users as $u){
            foreach($friends as $f){
                if($f->id == $u->id)
                    $users->shift($u->id);
            }
        }

        return $users;
    }

    public function invitations()
    {
        return $this->hasMany('Invitation','dest_id');
    }

    public function getFutureEvents()
    {
        return $this->allevenements()->where('from','>=', new DateTime('today'));
    }

    public function getPastEvents()
    {
        return $this->allevenements()->where('from','<', new DateTime('today'));
    }

    public function getName(){
        if(($this->prenom != "") && ($this->nom != ""))
            return $this->prenom.' '.$this->nom;
        else
            return $this->username;

    }

    public function notifications()
    {
        return $this->hasMany('Notification');
    }

    public function countNewNotifs()
    {
        return DB::table('notifications')->where('user_id', Auth::user()->id)->where('viewed', '0')->count();
    }

    public function countCreatedEvents()
    {
        return $this->evenements()->count();
    }

    public function countInvitations()
    {
        $count = 0;
        foreach($this->evenements()->get() as $event){
            $count += $event->invitations()->count();
        }
        return $count;
    }

    public function resetPassword()
    {
        //Fonction generate mdp
        $length = 10;
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $mdp = substr(str_shuffle($chars),0,$length);

        //setPassword
        $this->password = Hash::make($mdp);
        $this->save();
        return $mdp;
    }

}