<?php

class Home extends BaseController {

	/*
	| RESTFUL CONTROLLER OK
	*/

    public function __construct(){
        $this->beforeFilter('auth', array(
            'only' => 'getIndex',
        ));
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

	public function getIndex()
	{
		if(Auth::guest())
		{
			return Redirect::to('login');
		}
        $this->data['futureEvents'] = Auth::user()->getFutureEvents()->orderBy('from','asc')->get();
        $this->data['pastEvents'] = Auth::user()->getPastEvents()->orderBy('from','asc')->get();
        return View::make('index', $this->data);
	}

	public function getLogout()
	{
		//return 'logout';
		Auth::logout();
		return Redirect::to('login');
	}

	public function getLogin()
	{
		return View::make('login');
	}

    public function postRegister(){
        $user = new User;
        $user->username = Input::get('username');
        $user->set_password(Input::get('password'));
        $test_supinfo = '@supinfo.com';
        if(stristr(Input::get('email'),$test_supinfo))
        {
            $rest = substr(Input::get('email'),0,-12);
            if(is_numeric($rest))
            {
                $student = new Student;
                $student->opencampus_id = $rest;
                $student->save();
                $user->student()->associate($student);
            }
            else{}
        }
        $user->email = Input::get('email');
        $user->save();

        $data['key'] = $user->id.'-'.md5($user->email.$user->password);

        Mail::queue('emails.register', $data, function($message) use ($user)
        {
            $message->from('noreply@schrepel.me', 'SupEvents');
            $message->to($user->email);
            $message->subject('Welcome!');
        });

        Toastr::add('info','top-full-width','OK!','Now you have to activate your account. Please check your mails!');

        return Redirect::to('login');
    }

	public function postLogin()
	{
		$userdata = array(
			'username'=>Input::get('username'),
			'password'=>Input::get('password') 
		);

        $user = User::where('username',$userdata['username'])->first();

		if(Auth::attempt($userdata))
            if($user->activated == 1)
			    return Redirect::to('/');
            else{
                Toastr::add('error','top-full-width','Error!','You have to activate your account. Please check your mails!');
                return Redirect::to('login');
            }
		else
		{
            Toastr::add('error','top-full-width','Error!','Wrong username and/or password!');
            return Redirect::to('login')
			->withInput()->with('login_errors',true);
		}
	}

   public function postTheme()
   {
      $user = Auth::user();
      $user->theme = Input::get('theme');
      $user->save();
   }

    public function postVnotif()
    {
        $user = Auth::user();
        if($user->notifications()->count() > 0)
            foreach($user->notifications()->get() as $notif){
                $notif->viewed = 1;
                $notif->save();
            }
    }

    public function getActivate($key = false){

        $id = strstr($key, '-', true);
        $user = User::find($id);

        $hash = substr(strstr($key, '-'), 1, 32);

        $goodkey = md5($user->email.$user->password);

        if($hash == $goodkey){
            $user->activated = 1;
            $user->save();
            Auth::login($user);
            return Redirect::to('/');
        }
        else{
            return 'Wrong key !';
        }
    }

    public function postResetpass(){
        $email = Input::get('email');
        $user = User::where('email','=',$email)->first();
        if(!$user){
            Toastr::add('error','top-full-width','Error!','No account linked to this email');
            return Redirect::to('login');
        }

        $data['key'] = $user->id.'-'.md5($user->email.$user->password);

        Mail::queue('emails.resetpass', $data, function($message) use ($user)
        {
            $message->from('noreply@schrepel.me', 'SupEvents');
            $message->to($user->email);
            $message->subject('Reset Password!');
        });

        Toastr::add('info','top-full-width','Info!','Your password reset request has been sent! Please check your mails');
        return Redirect::to('login');
    }

    public function getRegen($key = false){
        $id = strstr($key, '-', true);
        $hash = substr(strstr($key, '-'), 1, 0);
        $user = User::find($id);

        $date = new DateTime();
        $hash1 = md5($date->format('c') . $user->email);
        $hash2 = md5($date->add(new DateInterval('P1D'))->format('c') . $user->email );

        if(in_array($hash, [$hash1, $hash2])){
            Toastr::add('error','top-full-width','Error!','The link has expired');
            return Redirect::to('login');
        }

        $password = $user->resetPassword();
        $data['password'] = $password;

        Mail::queue('emails.getpass', $data, function($message) use ($user)
        {
            $message->from('noreply@schrepel.me', 'SupEvents');
            $message->to($user->email);
            $message->subject('New Password!');
        });

        Toastr::add('success','top-full-width','Success!','Your new generated password has been sent. Check your mail!');
        return Redirect::to('login');

    }

}