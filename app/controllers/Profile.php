<?php

class Profile extends BaseController {

    public function __construct(){
        $this->beforeFilter('auth');
    }

	public function getIndex()
	{
		$this->data['countries'] = Country::orderBy('id','asc')->get();
		$this->data['user'] = Auth::user();
		$this->data['campuses']= Campus::orderBy('id','asc')->get();
		$this->data['classes']=Classes::orderBy('id','asc')->get();
		return View::make('profile',$this->data);
	}

    public function getShow($id)
    {
        $this->data['countries'] = Country::orderBy('id','asc')->get();
        $this->data['campuses']= Campus::orderBy('id','asc')->get();
        $this->data['classes']=Classes::orderBy('id','asc')->get();
        $this->data['user'] = User::find($id);
        return View::make('profile',$this->data);
    }

	public function postIndex()
	{
		$user = Auth::user();
		if(Input::has('prenom')) {$user->prenom = Input::get('prenom');}
		if(Input::has('nom')) {$user->nom = Input::get('nom');}
		if(Input::has('interests')) {$user->interests = Input::get('interests');}
		if(Input::has('occupation')) {$user->occupation = Input::get('occupation');}
		if(Input::has('country')) {$user->country_id = Input::get('country');}
		if(Input::has('description')) {$user->description = Input::get('description');}
		if(Input::has('school')) {$user->school = Input::get('school');}
		if(Input::has('website')) {$user->website = Input::get('website');}
    		$user->birth = Input::get('birth');		

		$user->save();
		return Redirect::to('profile');
	}

    public function postSupinfo(){
        $user = Auth::user();
        $student = $user->student;
        if(Input::has('campus')) {
            $campus = Campus::find(Input::get('campus'));
            $student->campus()->associate($campus);
        }
        if(Input::has('class')){
            $class = Classes::find(Input::get('class'));
            $student->classe()->associate($class);
        }

        $student->save();

        return Redirect::to('profile');
    }

    public function postChpass(){
        $user = Auth::user();

        if(Hash::check(Input::get('password'), $user->password)){
            if(Input::get('npassword') == Input::get('rnpassword')){
                $user->password = Hash::make(Input::get('npassword'));
                $user->save();
                Toastr::add('success','top-full-width','Success!','Password changed');
                return Redirect::to('profile');
            }else{
                Toastr::add('error','top-full-width','Error!','New passwords do not match');
                return Redirect::to('profile');
            }
        }else{
            Toastr::add('error','top-full-width','Error!','Wrong old password');
            return Redirect::to('profile');
        }
    }

}