<?php

use Illuminate\Database\Migrations\Migration;

class CreateActivities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('activities', function($table)
        {
            $table->increments('id');
            $table->integer('type_id');
            $table->text('comment');
            $table->string('redirect_uri');
            $table->integer('user_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activities');
	}

}