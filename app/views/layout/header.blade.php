<div class="header navbar navbar-inverse navbar-fixed-top">
<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="navbar-inner">
		<div class="container-fluid">
			<!-- BEGIN LOGO -->
			<a class="brand" href="/">
			<img src="/assets/img/logo.png" alt="logo" />
			</a>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
			<img src="/assets/img/menu-toggler.png" alt="" />
			</a>          
			<!-- END RESPONSIVE MENU TOGGLER -->				
			<!-- BEGIN TOP NAVIGATION MENU -->					
			<ul class="nav pull-right">
                <li class="dropdown" id="header_notification_bar">
                    <a class="dropdown-toggle" id="notification_button" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-warning-sign"></i>
                        <? if (Auth::user()->countNewNotifs() > 0): ?><span id="notif_badge" class="badge"><?= Auth::user()->countNewNotifs() ?></span><? endif; ?>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li>
                            <p>Notifications</p>
                        </li>
                        <li>
                            @if(Auth::user()->notifications()->count() > 0)
                            <ul class="dropdown-menu-list scroller" style="height: 250px;">
                                @foreach(Auth::user()->notifications()->orderBy('created_at','desc')->get() as $notif)
                                <li>
                                    <a href="{{$notif->link}}">
                                        <span class="label label-sm label-icon label-success"><i class="icon-user"></i></span>
                                        {{$notif->text}}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                    </ul>
                </li>
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						@if(is_object(Auth::user()->student))
						<img alt="" style="height:29px" src="http://www.campus-booster.net/actorpictures/<?=Auth::user()->student->opencampus_id?>.jpg" />
						@else
						<img alt="" style="height:29px" src="http://placehold.it/26x29"/>
						@endif
					<span class="username"> {{Auth::user()->username}}</span>
					<i class="icon-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="/profile"><i class="icon-user"></i> My Profile</a></li>
						<li><a href="/events"><i class="icon-calendar"></i> My Events</a></li>
						<li class="divider"></li>
						<li><a href="/logout"><i class="icon-key"></i> Log Out</a></li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
			</ul>
			<!-- END TOP NAVIGATION MENU -->	
		</div>
	</div>
<!-- END TOP NAVIGATION BAR -->
</div>
