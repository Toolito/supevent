@extends('layout.main')
@section('title')
Create an event
@endsection
@section('subtitle')
events
@endsection
@section('content')
<style type="text/css">
    .required{
        color: red;
    }
</style>
{{Form::open(array('url'=>'/events/add', 'method'=>'POST','class'=>'form-horizontal'))}}

<legend>Localization</legend>
<div class="row-fluid">
    <div class="span6" id="add_informations" style="display:none">
        <label>Address<span class="required">*</span></label>
        <p class="text-error">{{$errors->first('address')}}</p>
        <input name="address" type="text" id="address" class="m-wrap span12" value="Test"/>
        <span class="help-block">You can update address by clicking the map. </span>

        <label>The meeting place<span class="required">*</span></label>
        <p class="text-error">{{$errors->first('details')}}</p>
        <textarea name="details" rows="3" type="text" class="large m-wrap"></textarea>
        <span class="help-block">More details about the meeting place</span>

   </div>
   <div class="span6">
      <div class="portlet box red" id="map_puls">
         <div class="portlet-title">
            <div class="caption">Map</div>
            <div class="tools">
               <a href="javascript:;" class="collapse"></a>
               <a href="javascript:;" class="remove"></a>
            </div>
         </div>
         <div class="portlet-body">
            <div class="form-inline">
               <input type="text" data-original-title="Search by address" data-content="You can search a location and click the map to set the address of the event" data-placement="left" data-trigger="hover" id="gmap_placeevent_address" class="popovers m-wrap medium" placeholder="Address..." />
               <input type="button" id="gmap_placeevent_btn" class="btn" value="Search" />
            </div>
            <div id="gmap_placeevent" class="gmaps">
            </div>
         </div>
      </div>
   </div>
</div>
<legend>Date</legend>
<div class="row-fluid">
   <div class="span5">
      <label>From<span class="required">*</span></label>
       <p class="text-error">{{$errors->first('from-d')}}</p>
      <input name="from-d" type="text" class="datepicker m-wrap" />
      <span class="help-block">Click to select a date</span>
      <hr>
      <label>Hour<span class="required">*</span></label>
       <p class="text-error">{{$errors->first('from-h')}}</p>
      <div class="input-append bootstrap-timepicker-component">
         <input name="from-h" type="text" class="timepicker-24 m-wrap m-ctrl-small" />
         <span class="add-on"><i class="icon-time"></i></span>
         <span class="help-inline">{{$errors->first('from-h')}}</span>
      </div>
   </div>
   <div style="padding-top:8.5%;" class="span1">
      <i class="icon-chevron-right icon-4x"></i>
   </div>
   <div class="span5">
      <label>To</label>
      <input name="to-d" type="text" class="datepicker m-wrap" />
      <span class="help-block">Click to select a date</span>
      <hr>
      <label>Hour</label>
      <div class="input-append bootstrap-timepicker-component">
         <input name="to-h" type="text" class="timepicker-24 m-wrap m-ctrl-small" />
         <span class="add-on"><i class="icon-time"></i></span>
      </div>
   </div>
</div>
<div class="row-fluid">
   <div class="span7">
      <legend>Informations</legend>
      <div class="row-fluid">
         <label>Name<span class="required">*</span></label>
          <p class="text-error">{{$errors->first('name')}}</p>
         <input name="name" type="text" class="m-wrap" />
         <span class="help-block">Type the name of the event</span><br>

          <label>Description<span class="required">*</span></label>
          <p class="text-error">{{$errors->first('description')}}</p>
          <textarea class="wysihtml5 form-control" name="description" rows="6"></textarea>
          <span class="help-block">Describe the event</span><br>

         <label>Type</label>
          <select name="type" class="small m-wrap" tabindex="1">
              @foreach($eventTypes as $et)
              <option value="{{$et->id}}">{{$et->name}}</option>
              @endforeach
          </select><br>
      </div>
   </div>
   <div class="span5">
      <legend>Invitations</legend>
      <label>Users to invite</label>
      <select name="users[]" id="users_list" class="span6 select2" multiple>
         @foreach($users as $us)
         @if($us->prenom == "" || $us->nom == '')
         <option value="{{$us->id}}">{{$us->username}}</option>
         @else
         <option value="{{$us->id}}">{{$us->prenom}} {{$us->nom}}</option>
         @endif
         @endforeach
      </select>
   </div>
</div>
<div class="form-actions">
    <div style="width=300px" class="switch switch-large pull-left" data-on-label="PUB" data-off-label="PRIV" data-on="success" data-off="danger">
        <input type="checkbox" name="privacy" class="toggle" />
    </div>
   <button id="submit" class="pull-right btn big green" type="submit">Submit <i class="m-icon-big-swapright m-icon-white"></i></button>
</div>
{{Form::close()}}

@endsection
@section('styles')
<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="/assets/plugins/glyphicons/css/glyphicons.css" />
<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
<link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
<link href="/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css" media="screen"/>
<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
@endsection

@section('scripts')
<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
<!-- <script type="text/javascript" src="/assets/plugins/jquery.pulsate.min.js"></script> -->
<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="/assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script src="/assets/plugins/gmaps/gmaps.js"></script>
<!--END PAGE LEVEL PLUGINS -->
<script src="/assets/scripts/maps-google.js"></script>


<script>
   // jQuery(document).ready(function() {
   //    //FormWizard.init();

   // });
</script>
<script>
   jQuery(document).ready(function(){
      MapsGoogle.placeEvent();
      /*$('#map_puls').pulsate({
         color: 'red', // set the color of the pulse
         reach: 100,                              // how far the pulse goes in px
         speed: 1000,                            // how long one pulse takes in ms
         pause: 0,                               // how long the pause between pulses is in ms
         glow: true,                             // if the glow should be shown too
         repeat: 3,                           // will repeat forever if true, if given a number will repeat for that many times
         onHover: false                          // if true only pulsate if user hovers over the element
      });*/

      $('.datepicker').datepicker();

      $('.timepicker-24').timepicker({
         minuteStep: 15,
         showSeconds: false,
         showMeridian: false
      });

      $('#users_list').select2({
         placeholder: "Select users",
         allowClear: true
      });

   });
</script>
@endsection
