@extends('layout.main')
@section('title')
My friends
@endsection
@section('subtitle')
Friends
@endsection
@section('content')

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Username</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
        @foreach($friends as $f)
        <tr>
            <td>{{$f->id}}</td>
            <td>{{$f->prenom}}</td>
            <td>{{$f->nom}}</td>
            <td>{{$f->username}}</td>
            <td>
                <a href="/friends/chfriend/{{$f->id}}" class="btn mini red-stripe">Remove</a>
                <a href="/profile/show/{{$f->id}}" class="btn mini blue-stripe">Profile</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection