@extends('layout.main')
@section('title')
Dashboard
@endsection
@section('subtitle')
available soon
@endsection
@section('content')
<div class="row-fluid inbox">
    @include('shared.myevents')
    <div class="span10">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue tabbable">
            <div class="portlet-title">
                <div class="caption"><i class="icon-globe"></i>Events</div>
            </div>
            <div class="portlet-body">
                <div class="tabbable portlet-tabs">
                    <ul class="nav nav-tabs">
                        <li><a href="#pastEvents" data-toggle="tab">Past</a></li>
                        <li class="active"><a href="#futureEvents" data-toggle="tab">Upcoming</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="futureEvents">
                            @if(count($futureEvents) > 0)
                            <table class="table table-striped table-bordered table-hover table-full-width" id="eventsTable">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Type</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Going</th>
                                    <th>Invited</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($futureEvents as $e)
                                    <tr style="cursor: pointer" data-trigger="hover" data-original-title="Click to view this event." data-placement="left" class="tooltips" onclick="showEvent({{$e->id}})">
                                        <td>@if($e->privacy == 1) <i data-original-title="Private" class="tooltips icon-lock"></i> @else <i data-original-title="Public" class="tooltips icon-globe"></i> @endif</td>
                                        <td>{{$e->name}}</td>
                                        <td>{{$e->address}}</td>
                                        <td>{{$e->type->name}}</td>
                                        <td>{{date("m/d/Y H:i", strtotime($e->from))}}</td>
                                        <td>@if(date('Y',strtotime($e->to)) != 1970) {{date("m/d/Y H:i", strtotime($e->to))}} @endif</td>
                                        <td>{{$e->getGoingCount()}} <i data-original-title="@foreach($e->users()->get() as $u) {{$u->getName()}};  @endforeach" class="tooltips icon-user"></i></td>
                                        <td>{{count($e->getInvitedWoResult())}} <i data-original-title="@foreach($e->getInvitedWoResult() as $u) {{$u->getName()}};  @endforeach" class="tooltips icon-user"></i></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            No upcoming events
                            @endif
                        </div>
                        <div class="tab-pane" id="pastEvents">
                            @if(count($pastEvents) > 0)
                            <table class="table table-striped table-bordered table-hover table-full-width" id="eventsTable">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Type</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Going</th>
                                    <th>Invited</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pastEvents as $e)
                                <tr style="cursor: pointer" data-trigger="hover" data-original-title="Click to view this event." data-placement="left" class="tooltips" onclick="showEvent({{$e->id}})">
                                    <td>@if($e->privacy == 1) <i data-original-title="Private" class="tooltips icon-lock"></i> @else <i data-original-title="Public" class="tooltips icon-globe"></i> @endif</td>
                                    <td>{{$e->name}}</td>
                                    <td>{{$e->address}}</td>
                                    <td>{{$e->type->name}}</td>
                                    <td>{{date("m/d/Y H:i", strtotime($e->from))}}</td>
                                    <td>@if(date('Y',strtotime($e->to)) != 1970) {{date("m/d/Y H:i", strtotime($e->to))}} @endif</td>
                                    <td>{{$e->getGoingCount()}} <i data-original-title="@foreach($e->users()->get() as $u) {{$u->getName()}};  @endforeach" class="tooltips icon-user"></i></td>
                                    <td>{{count($e->getInvitedWoResult())}} <i data-original-title="@foreach($e->getInvitedWoResult() as $u) {{$u->getName()}};  @endforeach" class="tooltips icon-user"></i></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                            No past events
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('styles')
<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
<link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />
<link href="/assets/css/pages/inbox.css" rel="stylesheet" type="text/css" />

@endsection

@section('scripts')
<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
<script src="/assets/scripts/table-advanced.js"></script>
<script>
    function showEvent(id){
        return location.href="/events/view/"+id;
    }
    jQuery(document).ready(function() {
        TableAdvanced.init();
    });
</script>

@endsection
