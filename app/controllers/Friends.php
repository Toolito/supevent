<?php

class Friends extends BaseController {

    public function __construct(){
        $this->beforeFilter('auth');
    }

    public function getIndex()
    {
        $this->data['user'] = Auth::user();
        $this->data['friends'] = $this->data['user']->friends()->get();
        return View::make('friends.index', $this->data);
    }

    /*public function getIndex()
    {
        $this->data['countries'] = Country::orderBy('id','asc')->get();
        $this->data['user'] = Auth::user();
        $this->data['campuses']= Campus::orderBy('id','asc')->get();
        $this->data['classes']=Classes::orderBy('id','asc')->get();
        // Section::inject('title','My Profile');
        // Section::inject('subtitle','account management');
        return View::make('profile',$this->data);   
    }*/

    public function getAdd()
    {
        $user = Auth::user();
        $this->data['campus'] = Campus::orderBy('name','asc')->get();
        $results = $user->getNoFriends();
        return View::make('friends.add', $this->data);
    }

    public function postResults()
    {
        $user = Auth::user();
        //$users = $user->getNoFriends();
        $username = Input::get('username');
        $firstName = Input::get('firstName');
        $lastName = Input::get('lastName');
        $campus = Input::get('campus');
        if($username == "") $username = null;
        if($firstName == "") $firstName = null;
        if($lastName == "") $lastName = null;
        if($campus == "null") {
            $users = DB::table('users')->where('username','LIKE','%'.$username.'%')
            ->where('prenom','LIKE','%'.$firstName.'%')
            ->where('nom','LIKE','%'.$lastName.'%')
            ->where('id','!=',$user->id)
            ->get();  
        }else{
            $users = DB::table('users')->join('students',function($join){
                $join->on('users.student_id', '=', 'students.id');
            })
            ->where('users.username','LIKE','%'.$username.'%')
            ->where('users.prenom','LIKE','%'.$firstName.'%')
            ->where('users.nom','LIKE','%'.$lastName.'%')
            ->where('students.campus_id','=',Input::get('campus'))
            ->get();
        }


        return Response::json($users);
    }

    public function getChfriend($id)
    {
        $user = Auth::user();
        $friend = User::find($id);
        if($user->friends()->where('user_id','=',$user->id)->where('friend_id','=',$friend->id)->exists()){
            $user->friends()->detach($friend);
            $notification = new Notification();
            $notification->link = "/profile/show/".Auth::user()->id;
            $notification->user()->associate($friend);
            $notification->text = Auth::user()->getName().' removed you as friend.';
            $notification->save();
        }else{
            $user->friends()->attach($friend);
            $notification = new Notification();
            $notification->link = "/profile/show/".Auth::user()->id;
            $notification->user()->associate($friend);
            $notification->text = Auth::user()->getName().' added you as friend.';
            $notification->save();
        }
        return Redirect::to('/friends');
    }

}