@extends('layout.main')
@section('title')
User Profile
@endsection
@section('subtitle')
user profile sample
@endsection
@section('content')
<div class="row-fluid profile">
	<div class="span12">
		<!--BEGIN TABS-->
		<div class="tabbable tabbable-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab_1_1" data-toggle="tab">Overview</a></li>
				<li><a href="#tab_1_2" data-toggle="tab">Profile Info</a></li>
                @if(Request::segment(2) != "show")
				<li><a href="#tab_1_3" data-toggle="tab">Account</a></li>
                @endif
			</ul>
			<div class="tab-content">
				<div class="tab-pane row-fluid active" id="tab_1_1">
					<ul class="unstyled profile-nav span3">
						<li>
							@if(is_object($user->student))
							<img alt="" src="http://www.campus-booster.net/actorpictures/<?=$user->student->opencampus_id?>.jpg"/>
							@else
							<img alt="" src="/assets/img/avatars/default.png"/>
							@endif
							<a href="#" class="profile-edit">edit</a>
						</li>
						<li><a href="#">Messages <span>0</span></a></li>
						<li><a href="#">Friends <span>{{$user->friends()->count()}}</span></a></li>
					</ul>
					<div class="span9">
						<div class="row-fluid">
							<div class="span8 profile-info">
								<h1>@if(isset($user->prenom) && isset($user->nom)){{$user->prenom}} {{$user->nom}}@else {{$user->username}}@endif</h1>
								<p>{{$user->description}}</p>
								<p><a href="#">{{$user->website}}</a></p>
								<ul class="unstyled inline">
									<li><i class="icon-map-marker"></i>
										@if($user->country_id == 0)
										N/A
										@else
											{{$user->country->name}}
										@endif
									</li>
									<li><i class="icon-calendar"></i> {{$user->birth}}</li>
									<li><i class="icon-briefcase"></i> {{$user->occupation}}</li>
									<li><i class="icon-book"></i> {{$user->school}}</li>
									<li><i class="icon-heart"></i> {{$user->interests}}</li>
								</ul>
							</div>
							<!--end span8-->
							<div class="span4">
								<div class="portlet sale-summary">
									<div class="portlet-title">
										<h4>Events Summary</h4>
									</div>
									<ul class="unstyled">
										<li>
											<span class="sale-info">CREATED </span> 
											<span class="sale-num">{{$user->countCreatedEvents()}}</span>
										</li>
										<li>
											<span class="sale-info">PARTICIPATIONS </span> 
											<span class="sale-num">0</span>
										</li>
										<li>
											<span class="sale-info">INVITATIONS </span> 
											<span class="sale-num">{{$user->countInvitations()}}</span>
										</li>										
									</ul>
								</div>
							</div>
							<!--end span4-->
						</div>
						<!--end row-fluid-->
					</div>
					<!--end span9-->
				</div>
				<!--end tab-pane-->
				<div class="tab-pane profile-classic row-fluid" id="tab_1_2">
					<div class="span2">@if(is_object($user->student))
					<img alt="" src="http://www.campus-booster.net/actorpictures/<?=$user->student->opencampus_id?>.jpg"/>
					@else
					<img alt="" src="assets/img/avatars/avatar.png"/>
					@endif<a href="#" class="profile-edit">edit</a></div>
					<ul class="unstyled span10">
						<li><span>User Name:</span> {{$user->username}}</li>
@if(isset($user->prenom))<li><span>First Name:</span> {{$user->prenom}}</li>@else @endif
@if(isset($user->nom))<li><span>Last Name:</span> {{$user->nom}}</li>@else @endif
@if(isset($user->country_id))<li><span>Country:</span> {{$user->country->name}}</li>@else @endif
@if(isset($user->birth))<li><span>Birthday:</span> {{$user->birth}}</li>@else @endif
@if(isset($user->school))<li><span>School:</span> {{$user->school}}</li>@else @endif
@if(isset($user->occupation))<li><span>Occupation:</span> {{$user->occupation}}</li>@else @endif
@if(isset($user->email))<li><span>Email:</span> <a href="#">{{$user->email}}</a></li>@else @endif
@if(isset($user->interests))<li><span>Interests:</span> {{$user->interests}}</li>@else @endif
@if(isset($user->website))<li><span>Website Url:</span> <a href="#">{{$user->website}}</a></li>@else @endif
@if(isset($user->description))<li><span>About:</span> {{$user->description}}</li>@else @endif
					</ul>
				</div>
				<!--tab_1_2-->
                @if(Request::segment(2) != "show")
				<div class="tab-pane row-fluid profile-account" id="tab_1_3">
					<div class="row-fluid">
						<div class="span12">
							<div class="span3">
								<ul class="ver-inline-menu tabbable margin-bottom-10">
									<li class="active">
										<a data-toggle="tab" href="#tab_1-1">
										<i class="icon-cog"></i> 
										Personal info
										</a> 
										<span class="after"></span>                           			
									</li>
									<li class=""><a data-toggle="tab" href="#tab_2-2"><i class="icon-picture"></i> Change Avatar</a></li>
									<li class=""><a data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i> Change Password</a></li>
                                    @if(is_object($user->student))
									<li class=""><a data-toggle="tab" href="#tab_4-4"><i class="icon-eye-open"></i> SUPINFO</a></li>
									@endif
								</ul>
							</div>
							<div class="span9">
								<div class="tab-content">
									<div id="tab_1-1" class="tab-pane active">
										<div style="height: auto;" id="accordion1-1" class="accordion collapse">
											{{Form::open(array('url'=>'/profile', 'method'=>'POST'))}}
												<label class="control-label">First Name</label>
												<input name="prenom" type="text" value="{{$user->prenom}}" class="m-wrap span8" />
												<label class="control-label">Last Name</label>
												<input name="nom" type="text" value="{{$user->nom}}" class="m-wrap span8" />
												<label class="control-label">Birth</label>
                                				<input name="birth" class="m-wrap m-ctrl-medium datepicker" type="text" value="{{$user->birth}}"/>
												<label class="control-label">School</label>
												<input name="school" type="text" value="{{$user->school}}" class="m-wrap span8" />                                				
												<label class="control-label">Interests</label>
												<input name="interests" type="text" value="{{$user->interests}}" class="m-wrap span8" />
												<label class="control-label">Occupation</label>
												<input name="occupation" type="text" value="{{$user->occupation}}" class="m-wrap span8" />
												<label class="control-label">Country</label>
													<select name="country" class="medium m-wrap" tabindex="1">
														@foreach($countries as $country)
                                          				<option value="{{$country->id}}">{{$country->name}}
                                          				</option>
                                          				@endforeach
                                          			</select>
												<label class="control-label">About</label>
												<textarea name="description" class="span8 m-wrap" rows="3">{{$user->description}}</textarea>
												<label class="control-label">Website Url</label>
												<input name="website" type="text" value="{{$user->website}}" class="m-wrap span8" />
												<div class="submit-btn">
													<button type="submit" class="btn green">Save Changes</button>
													<a href="#" class="btn">Cancel</a>
												</div>
											</form>
										</div>
									</div>
									<div id="tab_2-2" class="tab-pane">
										<div style="height: auto;" id="accordion2-2" class="accordion collapse">
											<form action="#">
												<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</p>
												<br />
												<div class="controls">
													<div class="thumbnail" style="width: 291px; height: 170px;">
														<img src="http://www.placehold.it/291x170/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
													</div>
												</div>
												<div class="space10"></div>
												<div class="fileupload fileupload-new" data-provides="fileupload">
													<div class="input-append">
														<div class="uneditable-input">
															<i class="icon-file fileupload-exists"></i> 
															<span class="fileupload-preview"></span>
														</div>
														<span class="btn btn-file">
														<span class="fileupload-new">Select file</span>
														<span class="fileupload-exists">Change</span>
														<input type="file" class="default" />
														</span>
														<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="controls">
													<span class="label label-important">NOTE!</span>
													<span>You can write some information here..</span>
												</div>
												<div class="space10"></div>
												<div class="submit-btn">
													<a href="#" class="btn green">Submit</a>
													<a href="#" class="btn">Cancel</a>
												</div>
											</form>
										</div>
									</div>
									<div id="tab_3-3" class="tab-pane">
										<div style="height: auto;" id="accordion3-3" class="accordion collapse">
											{{Form::open(array('url'=>'/profile/chpass', 'method'=>'POST','id'=>'chpass-form'))}}
												<label class="control-label">Current Password</label>
												<input name="password" type="password" class="m-wrap span8" />
												<label class="control-label">New Password</label>
												<input name="npassword" type="password" class="m-wrap span8" />
												<label class="control-label">Re-type New Password</label>
												<input name="rnpassword" type="password" class="m-wrap span8" />
												<div class="submit-btn">
													<button type="submit" class="btn green">Change Password</button>
													<a href="#" class="btn">Cancel</a>
												</div>
											{{Form::close()}}
										</div>
									</div>
                                    @if(is_object($user->student))
									<div id="tab_4-4" class="tab-pane">			
										<div style="height: auto;" id="accordion1-1" class="accordion collapse">
											{{Form::open(array('url'=>'/profile/supinfo', 'method'=>'POST'))}}
												<label class="control-label">Campus Booster ID</label>
												<input class="small m-wrap" type="text" placeholder="{{$user->student->opencampus_id}}" disabled />
												<label class="control-label">Campus</label>
													<select name="campus" class="medium m-wrap" tabindex="1">
														@if(isset($user->student->campus->name))
														<option selected="selected" value="{{$user->student->campus->id}}">{{$user->student->campus->name}} ({{$user->student->campus->country->name}})</option> @endif
														@foreach($campuses as $campus)
			                                          <option value="{{$campus->id}}">{{$campus->name}} ({{$campus->country->name}})</option>
			                                          @endforeach
                                       </select>
												<label class="control-label">Class</label>
												<select name="class" class="small m-wrap" tabindex="1">
													@if(isset($user->student->classe->name))<option selected="selected" value="{{$user->student->classe->id}}">{{$user->student->classe->name}}</option> @endif
													@foreach($classes as $class)
													<option value="{{$class->id}}">{{$class->name}}</option>
													@endforeach
												</select>
												<div class="space15"></div>
												<div class="submit-btn">
													<button type="submit" class="btn green">Save Changes</button>
													<a href="#" class="btn">Cancel</a>
												</div>
											</form>
										</div>
									</div>
									@endif
								</div>
							</div>
							<!--end span9-->                                   
						</div>
					</div>
				</div>
                @endif
				<!--end tab-pane-->
			</div>
		</div>
		<!--END TABS-->
	</div>
</div>

	@section('styles')
	<link href="/assets/css/pages/profile.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	@endsection
	
	@section('scripts')
   <script type="text/javascript" src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script>
   $('.datepicker').datepicker({
   		format: 'dd/mm/yyyy'
   	});

    $('#chpass-form').validate({
	    errorElement: 'label', //default input error message container
	    errorClass: 'help-inline', // default input error message class
	    focusInvalid: false, // do not focus the last invalid input
	    ignore: "",
	    rules: {
	        password: {
	            required: true
	        },
	        npassword: {
	            required: true
	        },
	        rnpassword: {
	            equalTo: "#register_password"
	        }
	    },

	    invalidHandler: function (event, validator) { //display error alert on form submit   

	    },

	    highlight: function (element) { // hightlight error inputs
	        $(element)
	            .closest('.control-group').addClass('error'); // set error class to the control group
	    },

	    success: function (label) {
	        label.closest('.control-group').removeClass('error');
	        label.remove();
	    },

	    errorPlacement: function (error, element) {
	        error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	    },

	   /* submitHandler: function (form) {
	        //window.location.href = "index.html";
	    }*/
	});
   </script>
	@endsection
	
@endsection
