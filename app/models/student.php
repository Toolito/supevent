<?php
class Student extends Eloquent
{

	public function user()
	{
		return $this->hasOne('user');
	}

	public function classe()
	{
		return $this->belongsTo('Classes','class_id');
	}

	public function campus()
	{
		return $this->belongsTo('Campus','campus_id');
	}
}