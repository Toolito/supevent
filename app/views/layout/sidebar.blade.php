<?php

// Menu::handler('menu',array())->add('#','<i class="icon-white icon-user"></i>Villes',Menu::items('villes',array('class'=>'sub-menu'))->prefix_parents()
// 	->add('villes','Liste')
//     ->add('villes/add','Ajouter'),null,array('class'=>'has-sub'));

// Menu::handler('menu',array())->add('#','<i class="icon-white icon-home"></i>Projets',Menu::items('projets',array('class'=>'sub-menu'))
// 	->add('','Liste')
//     ->add('add','Ajouter'),null,array('class'=>'has-sub'));

// Menu::handler('menu',array())->add('#','<i class="icon-white icon-home"></i>Evenements',Menu::items('events',array('class'=>'sub-menu'))
// 	->add('','Liste')
//     ->add('add','Ajouter'),null,array('class'=>'has-sub'));

// Menu::handler('menu',array())->add('maps','<i class="icon-white icon-globe"></i>Cartes');

// Menu::handler('menu',array())->add('#','<i class="icon-white icon-home"></i>Wiki',Menu::items('wiki',array('class'=>'sub-menu'))
// 	->add('wiki','Accueil')
// 	->add('rules','Règles')       
//     ->add('cmds','Commandes'),null,array('class'=>'has-sub'));

// Menu::handler('menu',array())->add('admin','<i class="icon-white icon-cog"></i> Admin');


Menu::handler('menu',array('class'=>'page-sidebar-menu'))
    ->add('/','<i class="icon-white icon-home"></i>Home',null,null,array('class'=>'start'))
    ->add('#','<i class="icon-white icon-globe"></i>Events',Menu::items(null,array('class'=>'sub-menu'),'ul')
        ->add('/events','List')
        ->add('/events/add','Add'))
    ->add('#','<i class="icon-white icon-user"></i>Friends' ,Menu::items(null,array('class'=>'sub-menu'),'ul')
        ->add('/friends','List')
        ->add('/friends/add','Add'))
 
?>
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar nav-collapse collapse">
	<!-- END RESPONSIVE QUICK SEARCH FORM -->
	<!-- BEGIN SIDEBAR MENU -->
	<ul class="page-sidebar-menu">
		<li>
			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			<div class="sidebar-toggler hidden-phone"></div>
			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
		</li>
	</ul>

	 <?= Menu::handler('menu')->render(array('active_class'=>'active','active_child_class'=>'active')); ?>

	<!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->