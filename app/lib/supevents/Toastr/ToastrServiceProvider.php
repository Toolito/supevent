<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Toolito
 * Date: 12/11/13
 * Time: 11:33
 * To change this template use File | Settings | File Templates.
 */

namespace supevents\Toastr;

use Illuminate\Support\ServiceProvider;

class ToastrServiceProvider extends ServiceProvider {

    public function register()
    {
        //Register 'toastr' instance container to our Toastr object
        $this->app['toastr'] = $this->app->share(function($app)
        {
            return new Toastr;
        });

        //Shortcut alias
        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Toastr', 'supevents\Toastr\Facades\Toastr');
        });
    }

}