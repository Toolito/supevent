<?php
class Evenement extends Eloquent
{
    protected $table = "events";

    public function user(){
        return $this->belongsTo('User');
    }

    public function type(){
        return $this->belongsTo('EvenementType');
    }

    public function invitations(){
        return $this->hasMany('Invitation','event_id');
    }

    public function getInvitedCount(){
        return $this->invitations()->count();
    }

    public function getInvitedWoResult(){
        $users = array();
        foreach($this->invitations()->where('result','=','0')->get() as $invitation)
            array_push($users, $invitation->user()->first());

        return $users;
    }

    public function getInvited(){
        $users = array();
        foreach($this->invitations()->get() as $invitation)
            array_push($users, $invitation->user()->first());

        return $users;
    }

    public function getGoingCount(){
        return $this->users()->count();
    }

    public function users(){
        return $this->belongsToMany('User', 'user-event', 'event_id', 'user_id');
    }

    public function isAuthCreator(){
        if(Auth::user() == $this->user()->first())
            return true;
        else
            return false;
    }

}