var panel = $('.color-panel');

$('li',panel).click(function() {
    $(this).each(function() {
        $.post('/theme', {
            theme: $(this).attr("data-style")
        }, function(data){
        });
    });
});

$('#notification_button').click(function() {
    $.post('/vnotif', {
    }, function(data){
    });
    $('#notif_badge').empty();
});

var time = new Date();
var day_n = time.getDate();
var year = time.getFullYear();
var day;
var month;

switch(time.getDay())
{
    case 0:
        day = "Sunday";
        break;

    case 1:
        day = "Monday";
        break;

    case 2:
        day = "Tuesday";
        break;

    case 3:
        day = "Wednesday";
        break;

    case 4:
        day = "Thursday";
        break;

    case 5:
        day = "Friday";
        break;

    case 6:
        day = "Saturday";
        break;
}

switch(time.getMonth())
{
    case 0:
        month = "January"
        break;

    case 1:
        month = "February"
        break;

    case 2:
        month = "March"
        break;

    case 3:
        month = "April"
        break;

    case 4:
        month = "May"
        break;

    case 5:
        month = "June"
        break;

    case 6:
        month = "July"
        break;

    case 7:
        month = "August"
        break;

    case 8:
        month = "September"
        break;

    case 9:
        month = "October"
        break;

    case 10:
        month = "November"
        break;

    case 11:
        month = "December"
}

var time_formated = day + ", " + month + " " + day_n + " " + year;
$('#dashboard-report-range span').html(time_formated);