<div class="span2">
    <ul class="inbox-nav margin-bottom-10">
        <li class="compose-btn">
            <a class="btn green" href="/events/add">
                <i class="icon-plus"></i> Create
            </a>
        </li>
        @foreach($futureEvents as $e)
        @if(isset($event))
            @if($e->id == $event->id)
            <li class="active"><a href="/events/view/{{$e->id}}" class="btn">{{$e->name}}</a></li>
            @else
            <li><a href="/events/view/{{$e->id}}" class="btn">{{$e->name}}</a></li>
            @endif
        @else
        <li><a href="/events/view/{{$e->id}}" class="btn">{{$e->name}}</a></li>
        @endif
        @endforeach
    </ul>
</div>